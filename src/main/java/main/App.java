package main;
import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import java.util.HashMap;
import static spark.Spark.get;

public class App {

    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);

        //unsecure code
//        get("/form1", (req,res) -> {
//            HashMap<String, Object> model = new HashMap<>();
//            String templatePath = "/views/form1.vm";
//        // array of size 4.
//            int[] arr = new int[4];
//            try
//            {
//                int i = arr[4];
//                model.put("message","Inside try block");
//            }
//            catch(ArrayIndexOutOfBoundsException ex)
//            {
//                model.put("message","Exception caught in catch block");
//            }
//            finally
//            {
//                model.put("message","finally block executed");
//                return velocityTemplateEngine.render(new ModelAndView(model,
//                        templatePath));
//            }
//        });

        //Secure Code form 1
        get("/form1", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/form1.vm";
        // array of size 4.
            int[] arr = new int[4];
            try
            {
                int i = arr[4];
                System.out.println("TRY");
                model.put("message","Inside try block");
            }
            catch(ArrayIndexOutOfBoundsException ex)
            {
                System.out.println("CATCH");
                model.put("message","Exception caught in catch block");
            }
            finally
            {
                System.out.println("FINALLY");
                model.put("message","finally block executed");
            }
        // rest program will be executed
            model.put("message","Outside try-catch-finally clause");
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });


        // Code form 2
        get("/form2", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/form2.vm";
            Dimensions d = new Dimensions(8,8,8);
            model.put("message",d.getVolumePackage(21));
            model.put("message2", d.getVolumePackage(19));
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

//        //form 3 - UNSECURE CODE
//        get("/form3", (req,res) -> {
//            HashMap<String, Object> model = new HashMap<>();
//            String templatePath = "/views/form3.vm";
//            System.exit(1);
//            model.put("message","Status EXIT");
//        // ...
//            return velocityTemplateEngine.render(new ModelAndView(model,
//                    templatePath));
//        });

        get("/form3", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/form3.vm";
            PasswordSecurityManager secManager =
                    new PasswordSecurityManager();
            System.setSecurityManager(secManager);
            try {
                System.exit(1); // Abrupt exit call
            } catch (Throwable x) {
                if (x instanceof SecurityException) {
                    model.put("message","Intercepted System.exit()");
                    System.out.println("Intercepted System.exit()");
// Log exception
                } else {
// Forward to exception handler
                    model.put("message","This never executes");
                    System.out.println("This never executes");
                }
            }
            secManager.setExitAllowed(true); // Permit exit
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

    }

}
