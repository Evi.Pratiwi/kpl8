package main;

public class Dimensions {

    private int length;
    private int width;
    private int height;
    static public final int PADDING = 2;
    static public final int MAX_DIMENSION = 10;

    public Dimensions(int length, int width, int height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    //SECURE CODE (FINALLY CLAUSE)
//    protected int getVolumePackage(int weight) {
//        length += PADDING;
//        width += PADDING;
//        height += PADDING;
//        try {
//            if (length <= PADDING || width <= PADDING || height <= PADDING ||
//                    length > MAX_DIMENSION + PADDING || width > MAX_DIMENSION +
//                    PADDING ||
//                    height > MAX_DIMENSION + PADDING || weight <= 0 || weight >
//                    20) {
//                throw new IllegalArgumentException();
//            }
//            int volume = length * width * height;
//            return volume;
//        } catch (Throwable t) {
//            return -1; // Non-positive error code
//        } finally {
//            length -= PADDING; width -= PADDING; height -= PADDING;
//        }
//    }


    //SECURE CODE (INPUT VALIDATION)
//    protected int getVolumePackage(int weight) {
//        try {
//            if (length <= 0 || width <= 0 || height <= 0 ||
//                    length > MAX_DIMENSION || width > MAX_DIMENSION ||
//                    height > MAX_DIMENSION || weight <= 0 || weight > 20) {
//                throw new IllegalArgumentException();
//            }
//        } catch (Throwable t) {
//            return -1; // Non-positive error code
//        }
//        length += PADDING;
//        width += PADDING;
//        height += PADDING;
//        int volume = length * width * height;
//        length -= PADDING;
//        width -= PADDING;
//        height -= PADDING;
//        return volume;
//    }

    //UNSECURE CODE

//    protected int getVolumePackage(int weight) {
//        length += PADDING;
//        width += PADDING;
//        height += PADDING;
//        try {
//            if (length <= PADDING || width <= PADDING || height <= PADDING ||
//                    length > MAX_DIMENSION + PADDING || width > MAX_DIMENSION
//                    + PADDING ||
//                    height > MAX_DIMENSION + PADDING || weight <= 0 || weight
//                    > 20) {
//                throw new IllegalArgumentException();
//            }
//            int volume = length * width * height;
//            length -= PADDING;
//            width -= PADDING;
//            height -= PADDING; // Revert
//            return volume;
//        } catch (Throwable t) {
//            return -1; // Non-positive error code
//        }
//    }


    //SECURE CODE (UNMODIFIED OBJECT)
    protected int getVolumePackage(int weight) {
        try {
            if (length <= 0 || width <= 0 || height <= 0 ||
                    length > MAX_DIMENSION || width > MAX_DIMENSION ||
                    height > MAX_DIMENSION || weight <= 0 || weight > 20) {
                throw new IllegalArgumentException();
            }
        } catch (Throwable t) {
            return -1; // Non-positive error code
        }
        int volume = (length + PADDING) * (width + PADDING) *
                (height + PADDING);
        return volume;
    }

}
